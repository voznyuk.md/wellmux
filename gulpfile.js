var gulp = require('gulp'),
	sass = require('gulp-sass');
  prefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  cssmin = require('gulp-minify-css'),
	watch = require('gulp-watch'),
	notify = require('gulp-notify'),
	rimraf = require('rimraf'),
	browserSync  = require('browser-sync'),
  reload = browserSync.reload;


  var path = {
      dist: { //Куда класти готові файли для Distribution
          css:   'dist/css/',
          js:    'dist/js/',
          html:  'dist/',
          img:   'dist/imgs/',
          fonts: 'dist/fonts/'
      },
      src: { //Шлях звідки брати файли для зборки
          sass:  'src/style/style.scss',
          js:    'src/js/**/*.js',
          html:  'src/**/*.html',
          img:   'src/imgs/**/*',
          fonts: 'src/fonts/**/*'
      },
      watch: { //Шлях до файлів, за змінами яких ми наблюдаємо
          sass:  'src/style/**/*.scss',
          js:    'src/js/**/*.js',
          html:  'src/**/*.html',
          img:   'src/imgs/**/*',
          fonts: 'src/fonts/**/*'
      },
      clean: './dist'
  };

  gulp.task('browser-sync',['build'],function() {
  		browserSync({
  				server: {baseDir: "./dist"},
          host: 'localhost',
          port: 3000,
  				//tunnel: true,
          //notify: false
  				files: ['dist/**/*.html','dist/js/**/*.js','dist/css/**/*.css']
  		});
  });

  gulp.task('sass', function(){
    return gulp.src(path.src.sass)
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(prefixer())
      .pipe(cssmin())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(path.dist.css))
      .pipe(reload({stream: true}))
  });

  gulp.task('jsdist', function() {
    return gulp.src(path.src.js)
      .pipe(gulp.dest(path.dist.js))
      .pipe(reload({stream: true}))
  });

  gulp.task('htmldist', function() {
    return gulp.src(path.src.html)
      .pipe(gulp.dest(path.dist.html))
      .pipe(reload({stream: true}))
  });

  gulp.task('imgdist', function() {
    return gulp.src(path.src.img)
      .pipe(gulp.dest(path.dist.img))
      .pipe(reload({stream: true}))
  });

  gulp.task('fontsdist', function() {
    return gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.dist.fonts))
      .pipe(reload({stream: true}))
  });

  gulp.task('build', ['sass', 'htmldist', 'jsdist', 'imgdist', 'fontsdist']);

  gulp.task('watch', ['browser-sync'], function(){
    gulp.watch(path.watch.sass, ['sass']);
    gulp.watch(path.watch.js, ['jsdist']);
    gulp.watch(path.watch.html, ['htmldist']);
    gulp.watch(path.watch.img, ['imgdist']);
    gulp.watch(path.watch.fonts, ['fontsdist']);
  });

  gulp.task('clean', function (cb) {
      rimraf(path.clean, cb);
  });

  gulp.task('default', ['watch']);
